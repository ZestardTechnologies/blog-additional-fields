   <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                        <strong><span class="">How To Setup ?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>
                                    </p>
                                </h4>
                            </div>

                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Now you can see the custom meta fields label and values in your way.</p>

                                    <ul class="ul-help">

                                        <p> 1) First Add the custom meta fields click on <a href="https://www.zestardshop.com/shopifyapp/category_extra_fields/zestard/Categories/category_settings">"Add-fields"</a> Tab.After that open page click on Add Fields button.</p>
                                        <ul>
                                            <li>Clicking on Add fields button add the Custom Fields According to Your Requirements.</li>
                                            <li>click on save button.</li>
                                            <li>According to choosen fields each and every fields has one short-code generates.</li>
                                        </ul>
                                        <br>
                                        <p> 2) Once you add fields your fields, After that you can see the multiple tabs over there naviagation bar.
                                            Select or click on any naviagation tab.Below mention that links.</p>
                                        <ul>
                                            <li> 1) If you are display the values of custom meta fields in category front view click on <a href="https://shopifydev.anujdalal.com/dev_metafields/public/collections">Collection Tab</a> and fill up the fields values click on Edit button.</li>
                                            <li> 2) If you are display the values of custom meta fields in pages front view click on <a href="https://shopifydev.anujdalal.com/dev_metafields/public/pages">Pages Tab</a> and fill up the fields values click on Edit button.</li>
                                            <li> 3) If you are display the values of custom meta fields in blogs front view click on <a href="https://shopifydev.anujdalal.com/dev_metafields/public/blogs">Blogs Tab</a> and fill up the fields values click on Edit button.</li>
                                            <li> 4) If you are display the values of custom meta fields in Articals front view click on <a href="https://shopifydev.anujdalal.com/dev_metafields/public/articles">Articals Tab</a> and fill up the fields values click on Edit button.</li>
                                            <li> 5) If you are display the values of custom meta fields in Customers front view click on <a href="https://shopifydev.anujdalal.com/dev_metafields/public/customers">Customers Tab</a> and fill up the fields values click on Edit button.</li>
                                            <li> 5) If you are display the values of custom meta fields in Products front view click on <a href="https://shopifydev.anujdalal.com/dev_metafields/public/products">Products Tab</a> and fill up the fields values click on Edit button.</li>
                                            <li> 6) Whatever the Fields are required, must be chosen that fields value.</li>
                                            <li> 7) If you don't want to display selected fields just go on Add-Fields Tab and click on delete-Item button and click on save button.</li>
                                            <li> 8) Then You come back the edit product section Now you can not see that fields in Edit Product page view.</li>
                                            <li> 9) If you want to choose date,time and date-time fields then you have to set the global configuration.</li>
                                            <li> 10) Click on global-config Tab and set the fields data according to requirement.</li>
                                        </ul>
                                        <br>

                                        <p>3) Front view showing values for that add this JS file.<br></p>
                                        <ul>
                                            <li>Go to Online Store =&gt; Theme =&gt; Press (...) button =&gt; Select Edit HTML/CSS =&gt; Click on the Theme. Liquid on Layout section</li>  						
                                            <li>Then add this js 
                                                <div class="showCodeWrapperarea">
                                                    <button type="button" class="btn btn-default copyarea" onclick="copyToClipboard()" style="display: block;"><i class="fa fa-check"></i> Copied</button>
                                                    <textarea id="shortcode" rows="3" class="form-control short-code" data-app-type="banner-slider" readonly="">&lt;script type="text/javascript" src="https://zestardshop.com/category_extra_fields/zestard/zestard_custom_category.js" &gt;&lt;/script&gt;</textarea>
                                                </div>
                                                between &lt;head&gt;&lt;/head&gt; tag.
                                            </li>
                                        </ul>
                                      

                                    </ul>
                                </div>
                            </div>
                        </div>