<div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
    <div class="panel-group" id="accordion">
        <div class ="row">
            <div class ="col-sm-6">
                <ul class="ul-help">
                    <ul>
                        <li>To uninstall the app, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                        <li>Click on delete icon of <?php echo $app_type; ?> Additional Fields App.</li>
                        <li>If possible remove the shortcode from <b>liquid</b> files.</li>
                    </ul>
                </ul>
            </div>
            <div class ="col-sm-6">
                <div class ="screenshot_box">
                    <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/help/help_006.png') }}">
                        <img class="img-responsive" src="{{ asset('image/help/help_006.png') }}">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>