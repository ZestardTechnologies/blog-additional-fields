<ul class="ul-help"><div class="row">
                    <div class ="col-sm-6">
                        <p><b><?php echo $app_type_singular; ?> List</b></p>
                        <p>Each of the <?php echo $app_type; ?> in your store, can have a different value for the meta fields added using add fields option.</p>
                        <ul>
                            <li>Click on main <b><a href="https://<?php echo $_SERVER['HTTP_HOST'] ?><?php echo $dashboard_route; ?>" target="_blank"> <?php echo $app_type; ?> Tab</a></b> and go to <b><?php echo $app_type; ?> List</b> Tab. The list of all <?php echo $app_type; ?> in your store will be displayed.</li>
                            <li>You can edit any <?php echo $app_type; ?> you want to add meta fields value for.</li>
                            <li>Once you add meta fields value for a <?php echo $app_type_singular; ?>, and copy the shortcodes in 
                                <?php echo $app_type.".liquid";?> file as described below, the meta fields will be displayed in store on <?php echo $app_type.".liquid";?> page.</li>
                        </ul>
                    </div>

                    <div class ="col-sm-6">
                        <div class ="screenshot_box">
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/help/help_003.png') }}" target="_blank">
                                <img class="img-responsive" src="{{ asset('image/help/help_003.png') }}">
                            </a>
                            <br/>
                            <br/>
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/help/help_004.png') }}" target="_blank">
                                <img class="img-responsive" src="{{ asset('image/help/help_004.png') }}">
                            </a>
                        </div>
                    </div>
                </div>
</ul>