<ul class="nav nav-tabs" id="parentTab">
    <li class="active"><a data-toggle="tab" href="#collection">Collections</a></li>
    <li><a data-toggle="tab" href="#pages">Pages</a></li>
    <li><a data-toggle="tab" href="#blogs">Blogs</a></li>
    <li><a data-toggle="tab" href="#articles">Articles</a></li>
    <li><a data-toggle="tab" href="#orders">Orders</a></li>
    <li><a data-toggle="tab" href="#customers">Customers</a></li>
</ul>
