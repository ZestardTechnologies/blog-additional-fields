<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                <strong><span class="">Collection/Category</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <p>Now you can see the custom meta fields label and values in your way.</p>
            <p> 4) As Per above discussion each fields short-code generated put in to diffent -different file like you have to display the category page then put the shortcode collection.liquid file. </p>
            <p>    Same as view blog put it on blog.liquid file.</p>
            <p>    Same as view artical put it on artical.liquid file.</p>
            <p>    Same as view pages put it on page.liquid file.</p>
            <p>    Same as view customer put it on customer/account.liquid file.</p>
            <p>    Same as view order put it on customer/order.liquid file.</p>

            <ul>
                <li>You have to display label and value in front view</li>
                <li>Then put the short-code format </li>
                <li>copy and paste shortcode for collection or category, 
                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_collection_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_collection_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_collections" id="collection.id"></div>' ?> </textarea></div>
                    </div>

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_collection_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_collection_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_collection_fields" id="Your short-code">[label] : [value]</div>' ?> </textarea></div>
                    </div>                                                

                </li>


                <li>Don't change class name for every short-codes.</li>
                <li>Your id will be your short-code</li>
            </ul>



            <p>Copy and paste the following code into head part of your <a href="<?php echo 'https://' . session('shop') . '/admin/themes/current/?key=layout/theme.liquid' ?>" target="_blank"><b>Theme.liquid</b></a> file.</p>

            <div class="copystyle_wrapper col-md-9">
                <textarea rows="1" class="form-control script_code" id="script_code" disabled><?php echo "{{ '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js' | script_tag }}" ?></textarea>
                <btn value="Copy Shortcode" class="btn btn-info copycss_button tooltipped tooltipped-s copyMe" style="display: block;" onclick="copyToClipboard('#script_code')"><i class="fa fa-check"></i> Copy</btn>
            </div>
        </div>
    </div>

</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                <strong><span class="">Pages</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">


            <li>copy and paste shortcode for Pages, 
                <div class="form-group">
                    <div class="showCodeWrapperarea">
                        <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_page_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                        <textarea id="shortcode_page_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_pages" id="page.id"></div>' ?> </textarea></div>
                </div>
                
                
                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_page_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_page_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_page_fields" id="Your short-code">[label] : [value]</div> ' ?> </textarea></div>
                    </div>


            </li>
        </div>
    </div>

</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                <strong><span class="">Blogs</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse4" class="panel-collapse collapse">
        <div class = "panel-body">
            
                <li>copy and paste shortcode for Blogs, 

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_blog_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_blog_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_blogs" id="blog.id"></div>' ?> </textarea></div>
                    </div>

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_blog_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_blog_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_blog_fields" id="Your short-code">[label] : [value]</div>' ?> </textarea></div>
                    </div>


                </li>
        </div>
    </div>

</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse5"> 
                <strong><span class="">Articles</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse5" class="panel-collapse collapse">
        <div class = "panel-body">
                            <li>copy and paste shortcode for Articals, 

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_artical_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_artical_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_articals" id="article.id"></div>' ?> </textarea></div>
                    </div>

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_artical_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_artical_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_articals_fields" id="Your short-code">[label] : [value]</div>' ?> </textarea></div>
                    </div>



                </li>
        </div>
    </div>

</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse6"> 
                <strong><span class="">Orders</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse6" class="panel-collapse collapse">
        <div class = "panel-body">
            
                <li>copy and paste shortcode for Orders, 
                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_order_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_order_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_orders" id="order.id"></div>' ?> </textarea></div>
                    </div>

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_order_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_order_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_orders_fields" id="Your short-code">[label] : [value]</div>' ?> </textarea></div>
                    </div>

                </li>
        </div>
    </div>

</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse7"> 
                <strong><span class="">Customers</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse7" class="panel-collapse collapse">
        <div class = "panel-body">
                            <li>copy and paste shortcode for customer,                                                

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_customer_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_customer_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_customers" id="customer.id"></div>' ?> </textarea></div>
                    </div>



                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_customer_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_customer_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_customer_fields" id="Your short-code">[label] : [value]</div>' ?> </textarea></div>
                    </div>
                </li>
        </div>
    </div>

</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <p data-toggle="collapse" data-parent="#accordion" href="#collapse8"> 
                <strong><span class="">Products</span>
                    <span class="fa fa-chevron-down pull-right"></span></strong>
            </p>
        </h4>
    </div>

    <div id="collapse8" class="panel-collapse collapse">
        <div class = "panel-body">
            
                <li>copy and paste shortcode for product,                                                

                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_customer_id" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_customer_id" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="multi_custom_products" id="product.id"></div>' ?> </textarea></div>
                    </div>


                    <div class="form-group">
                        <div class="showCodeWrapperarea">
                            <button type="button" class="btn btn-default copyMe" data-clipboard-target="#shortcode_customer_fields" style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
                            <textarea id="shortcode_customer_fields" rows="1" class="form-control short-code" data-app-type="banner-slider" readonly=""><?php echo '<div class="custom_products_fields" id="Your short-code">[label] : [value]</div>' ?> </textarea></div>
                    </div>

                </li>
        </div>
    </div>

</div>