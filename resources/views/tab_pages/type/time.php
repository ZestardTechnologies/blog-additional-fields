<label  class="control-label col-sm-4" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em class="text-danger">*</em>': '>'.$row->title; ?></label>
<div class="col-sm-offset-2 col-sm-6 field">
<?php
    $is_required = ($row->is_require)? 'required' : '';
    $class = $row->type.$row->field_id;
    
    $time_format = (isset($config[0]->time_format) && $config[0]->time_format != '') ? $config[0]->time_format : '1';
?>
<div class="input-group date <?php echo $class; ?> col-md-12" data-date-format="HH:ii P" data-link-field="dtp_input3" data-link-format="HH:ii P">
	<input class="<?php echo ($row->is_require) ? 'required-entry':''; ?> form-control" size="16" type="text" name="<?php echo 'custom['.$row->field_id .'][values]'; ?>" value="<?php echo $values; ?>">
	<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
	<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
</div>
    
<script type="text/javascript">
<?php if($time_format == '1') { ?>
$(document).ready(function(){
    var today = new Date();
    $('.<?php echo $class; ?>').datetimepicker({
            weekStart: 0,
            todayBtn:  0,
            setStartDate : today,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            showMeridian: true,
            maxView: 1,
            forceParse: 1
    });
});
<?php } else { ?>
$('.<?php echo $class; ?>').datetimepicker({
	language:  'us',
	weekStart: 0,
	todayBtn:  0,
	autoclose: 1,
	todayHighlight: 1,
	startView: 1,
	minView: 0,
	showMeridian: true,
	maxView: 1,
	forceParse: 1
});
<?php } ?>
</script>
</div>
