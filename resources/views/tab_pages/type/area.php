<label  class="control-label col-sm-4" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em>*</em>': '>'.$row->title; ?></label>
<div class="col-sm-offset-2 col-sm-6 field">
    <textarea id="<?php echo $row->type.$row->field_id ?>" class="<?php echo ($row->is_require) ? 'required-entry':''; ?> form-control" name="<?php echo 'custom['.$row->field_id.'][values]'; ?>" rows="5" cols="25" <?php echo ($row->max_characters != '') ? 'maxlength="' . $row->max_characters . '"' : ''; ?>><?php echo $values; ?></textarea>
</div>