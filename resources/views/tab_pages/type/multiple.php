<div class="form-group">
<label  class="control-label col-sm-4" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em> *</em>': '>'.$row->title; ?></label>
<div class="col-sm-offset-2 col-sm-6 field"><select name="<?php echo 'custom['.$row->field_id .'][values][]'; ?>" class="form-control <?php echo ($row->is_require) ? 'required-entry':''; ?>" id="<?php echo $row->type.$row->field_id ?>" title="<?php echo $row->title; ?>" multiple="multiple" >
<?php
$childoptions = unserialize($row->child_options);
$cnt = 1;
$is_required = ($row->is_require)? 'required-entry' : '';
foreach($childoptions as $option){
        $selected = (in_array($cnt,explode(',',$values))) ? 'selected' : '';
	echo '<option value="'.$cnt.'" '.$selected.' >'.$option['title'].'</option>';
	$cnt++;
}
?>
</select>
</div>
</div>