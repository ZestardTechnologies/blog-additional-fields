<label class="control-label col-sm-4" for="<?php echo $row->type.$row->field_id ?>" <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em> *</em>': '>'.$row->title; ?></label>
<div class="col-sm-offset-2 col-sm-6 field"><select name="<?php echo 'custom['.$row->field_id .'][values]'; ?>" class="<?php echo ($row->is_require) ? 'required-entry':''; ?> form-control" id="<?php echo $row->type.$row->field_id ?>" title="<?php echo $row->title; ?>" <?php echo ($row->is_require) ? 'required':''; ?>>
<option value="">-- Please Select --</option>
<?php
$childoptions = unserialize($row->child_options);
$cnt = 1;
foreach($childoptions as $option){
        $selected = ($values == $cnt) ? 'selected' : '';
	echo '<option value="'.$cnt.'" '.$selected.'>'.$option['title'].'</option>';
	$cnt++;
}
?>
</select>
</div>