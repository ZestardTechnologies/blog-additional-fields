<label class="control-label col-sm-4"  <?php echo ($row->is_require) ? 'class="required">'.$row->title.'<em> *</em>': '>'.$row->title; ?></label>
<div class="col-sm-offset-2 col-sm-6 field">
<?php
    $childoptions = unserialize($row->child_options);
    $cnt = 1;
    $is_required = ($row->is_require)? 'required-entry' : '';
?>
<div class="<?php echo $is_required; ?> selection">
<?php
foreach($childoptions as $option){
    $checked = ($values == $cnt)? 'checked':'';
    echo '<label class="radio-inline"><input type="radio" class="option" name="custom['.$row->field_id.'][values]" id="fields_'.$row->field_id.'_'.$cnt.'"  value="'.$cnt.'" '.$checked.'>'.$option['title'].'</label>';    
    $cnt++;
}
?>
</div>
</div>