<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class HelpController extends Controller {
    
    public function index()
    {
        $app_type = "Blogs";
        $app_type_singular = "Blog";
        $dashboard_route = "/shopifyapp/blog-additional-fields/public/blogs";
        $liquid_file_path = "/admin/themes/current/?key=sections/blog-template.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_blogs" id="{{blog.id}}"></div>&#13;&#10;<div class="custom_blog_fields" id="shortcode_blog_fields_title"><span>[label]:</span> <span>[value]</span></div>&#13;&#10;</div>';
        
        $field_shortcode = '<div class="custom_blog_fields" id="shortcode_blog_fields_title"><span>[label]:</span> <span>[value]</span></div>';
        return view('help.help', compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
    
    public function appConfiguration()
    {
        $app_type = "Blogs";
        $app_type_singular = "Blog";
        $dashboard_route = "/shopifyapp/blog-additional-fields/public/blogs";
        $liquid_file_path = "/admin/themes/current/?key=sections/blog-template.liquid";
        $main_shortcode = '<div id="metafields" class="additional_css">&#13;&#10;<h2 class="additional_title"></h2>&#13;&#10;<div class="multi_custom_blogs" id="{{blog.id}}"></div>&#13;&#10;<div class="custom_blog_fields" id="shortcode_blog_fields_title"><span>[label]:</span> <span>[value]</span></div>&#13;&#10;</div>';
        
        $field_shortcode = '<div class="custom_blog_fields" id="shortcode_blog_fields_title"><span>[label]:</span> <span>[value]</span></div>';
        return view('app_configuration',compact('dashboard_route' , 'app_type' ,'app_type_singular','liquid_file_path','main_shortcode','field_shortcode'));
    }
}